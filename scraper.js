// Other Sites to try ...
// http://www.tageo.com/index-e-nz-cities-NZ.htm
//
//

let cheerio = require('cheerio');
let request = require('request');
let fs      = require('fs');
var dir     = './data/';

request("https://www.mapsofworld.com/lat_long/newzealand-lat-long.html", function (error, response, html) {
    if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);

        $('.table_con').each(function(index, element) {
            let data = $(element).find('td');
            let location = {};
            let locations = [];
			let JSONlocations = [];

            for (let i = 0; i < data.length; i ++) {
                let locationData = $(data[i]).text().trim().replace(/[!@#$%^&,.]/g, "");
                if (i % 3 === 0) {
                    location["Name"] = locationData;
                } else if (i % 3 === 1) {
                    location["Latitude"] = parseLatLng(locationData);
                } else {
                    location["Longitude"] = parseLatLng(locationData);

					// Strip the results that grabbed the header instead of actual data ...
					if (location.Name != "Locations") {
						JSONlocations.push(location);
						locations.push(location.Name);
					}
                    location = {};
                }
            }

			if (!fs.existsSync(dir)) {
				fs.mkdirSync(dir);
			}
			
			// Create JSON with lat/lng data ...
			fs.writeFile("./data/locations.json", JSON.stringify(JSONlocations, null, 2), function(err) {
				if (err) {
					return console.log(err);
				}
			});

			// Create text file with list of names ...
			fs.writeFile("./data/names.txt", JSON.stringify(locations), function(err) {
				if (err) {
					return console.log(err);
				}
			});
        });
    } else {
        console.warn("Error: " + response.statusCode + " " + response.statusMessage);
    }
});

function parseLatLng(value) {
    return ConvertDMSToDD(
        value.split("°")[0],
        value.split("'")[0].split("°")[1],
        value.split("'")[1]
    );
}

function ConvertDMSToDD(degrees, minutes, direction) {
    degrees = parseFloat(degrees);
    minutes = parseFloat(minutes);

    var dd = degrees + minutes / 60 + 0 / (60 * 60);

    if (direction == "S" || direction == "W") {
        dd = dd * -1;
    }

    return dd;
}